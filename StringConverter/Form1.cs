﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace StringConverter
{
    public partial class Form1 : Form
    {
        string srcAndroidFile;
        string srcWinFile;

        Dictionary<string, string> baseMap = new Dictionary<string, string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            listFolders.Items.Clear();

            textBoxLog.Text = "";

            if (textBoxPathAndroid.Text.Length == 0 || textBoxPathWin.Text.Length == 0)
                return;

            string[] folders = Directory.GetDirectories(textBoxPathAndroid.Text, "values*", SearchOption.AllDirectories);
            for (int i = 0; i < folders.Length; i++)
            {
                listFolders.Items.Add(folders[i], true);
            }

            srcAndroidFile = GetAndroidSourceFile();

            if (!File.Exists(srcAndroidFile))
            {
                textBoxLog.AppendText("File " + srcAndroidFile + " not exist" + Environment.NewLine);
                return;
            }
            srcWinFile = GetWinSourceFile();

            if (!File.Exists(srcWinFile))
            {
                textBoxLog.AppendText("File " + srcWinFile + " not exist" + Environment.NewLine);
                return;
            }

            CreateMap();

            if (baseMap.Count == 0)
            {
                textBoxLog.AppendText("No identical strings found" + Environment.NewLine);
                return;
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < listFolders.Items.Count; i++)
            {
                if (listFolders.GetItemCheckState(i) == CheckState.Checked)
                    Convert(listFolders.Items[i].ToString());
            }

            textBoxLog.AppendText("Done");
        }

        private void Convert(string srcFolder)
        {
            string srcFile = srcFolder + "\\strings.xml";
            if (srcFile.Equals(srcAndroidFile))
                return;

            int idx = srcFolder.LastIndexOf("\\values");
            if (idx == 0)
                return;

            string sub = srcFolder.Substring(idx);

            idx = sub.LastIndexOf("-");
            if (idx == 0)
                return;

            string lang = sub.Substring(idx+1);

            try
            { 
                string destFile = CreateWinResFileName(lang);

                if (File.Exists(srcFile))
                    Convert(srcFile, destFile);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: {0}", ex.Message);
            }     
        }

        private void Convert(string srcFile, string destFile)
        {
            string xmlFile1 = File.ReadAllText(srcFile);
            XmlDocument xml1 = new XmlDocument();
            xml1.LoadXml(xmlFile1);

            string xmlFile2 = File.ReadAllText(srcWinFile);
            XmlDocument xml2 = new XmlDocument();
            xml2.LoadXml(xmlFile2);

            XmlNodeList xList1 = xml1.SelectNodes("/resources/string");
            XmlNodeList xList2 = xml2.SelectNodes("root/data");

            int counter = 0;

            foreach (XmlNode xNode in xList1)
            {
                try
                {
                    string value = xNode.InnerText;
                    XmlNode xName = xNode.Attributes.GetNamedItem("name");

                    var nameToConvert = baseMap[xName.InnerText];
                    foreach (XmlNode xNode2 in xList2)
                    {
                        XmlNode xName2 = xNode2.Attributes.GetNamedItem("name");
                        if (xName2.Value == nameToConvert)
                        {

                            xNode2.SelectSingleNode("value").InnerText = value;
                            counter++;
                            break;
                        }
                    }
                }
                catch (KeyNotFoundException ex)
                {
                    Debug.WriteLine("Exception: {0}", ex.Message);
                }
            }

            xml2.Save(destFile);

            string msg = string.Format("\n{0}: strings changed: {1}", destFile, counter);
            textBoxLog.AppendText(msg + Environment.NewLine);
        }

        private string CreateWinResFileName(string lang)
        {
            string file = textBoxPathWin.Text;

            if (file.LastIndexOf("\\") != file.Length - 1)
                file += "\\";

            file += lang;

            DirectoryInfo di = Directory.CreateDirectory(file);

            file += "\\Resources.resw";

            return file;
        }

        private string GetAndroidSourceFile()
        {
            string file = textBoxPathAndroid.Text;

            if (file.LastIndexOf("\\") != file.Length - 1)
                file += "\\";

            if (textBoxLangAndroid.Text.Length > 0)
            {
                file += textBoxLangAndroid.Text;
            }
            else
            {
                file += "values";
            }

            file += textBoxLangAndroid.Text;
            if (file.LastIndexOf("\\") != file.Length - 1)
                file += "\\";

            file += "strings.xml";

            return file;
        }

        private string GetWinSourceFile()
        {
            string file = textBoxPathWin.Text;

            if (file.LastIndexOf("\\") != file.Length - 1)
                file += "\\";

            file += textBoxLangWin.Text;
            if (file.LastIndexOf("\\") != file.Length - 1)
                file += "\\";

            file += "Resources.resw";

            return file;
        }

        private void CreateMap()
        {
            baseMap.Clear();

            try
            {
                string xmlFile1 = File.ReadAllText(srcAndroidFile);
                XmlDocument xml1 = new XmlDocument();
                xml1.LoadXml(xmlFile1);


                string xmlFile2 = File.ReadAllText(srcWinFile);
                XmlDocument xml2 = new XmlDocument();
                xml2.LoadXml(xmlFile2);


                XmlNodeList xList1 = xml1.SelectNodes("/resources/string");
                XmlNodeList xList2 = xml2.SelectNodes("root/data");

                foreach (XmlNode xNode in xList1)
                {
                    string value = xNode.InnerText;
                    var xName = xNode.Attributes.GetNamedItem("name");

                    foreach (XmlNode xNode2 in xList2)
                    {
                        XmlNode xName2 = xNode2.Attributes.GetNamedItem("name");
                        XmlNode xVal = xNode2.SelectSingleNode("value");

                        if (value == xVal.InnerText)
                        {
                            baseMap.Add(xName.InnerText, xName2.Value);
                            break;
                        }
                    }

                }

                string msg = string.Format("\nFound identical strings: {0}. All strings: {1}", baseMap.Count, xList2.Count);
                textBoxLog.AppendText(msg + Environment.NewLine);
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Exception: {0}", ex.Message);
            }

        }

        private void btnCheckAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listFolders.Items.Count; i++)
                listFolders.SetItemChecked(i, true);

        }

        private void btnCheckNone_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listFolders.Items.Count; i++)
                listFolders.SetItemChecked(i, false);
        }
    }
}
