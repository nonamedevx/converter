﻿namespace StringConverter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPathAndroid = new System.Windows.Forms.TextBox();
            this.textBoxPathWin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnConvert = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxLangAndroid = new System.Windows.Forms.TextBox();
            this.textBoxLangWin = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.listFolders = new System.Windows.Forms.CheckedListBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCheckAll = new System.Windows.Forms.Button();
            this.btnCheckNone = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Path to android res folder:";
            // 
            // textBoxPathAndroid
            // 
            this.textBoxPathAndroid.Location = new System.Drawing.Point(188, 29);
            this.textBoxPathAndroid.Name = "textBoxPathAndroid";
            this.textBoxPathAndroid.Size = new System.Drawing.Size(387, 20);
            this.textBoxPathAndroid.TabIndex = 1;
            this.textBoxPathAndroid.Text = "D:\\Projects\\efmsoft\\Games\\Puzzler\\Android\\Puzzler\\src\\main\\res";
            // 
            // textBoxPathWin
            // 
            this.textBoxPathWin.Location = new System.Drawing.Point(188, 74);
            this.textBoxPathWin.Name = "textBoxPathWin";
            this.textBoxPathWin.Size = new System.Drawing.Size(387, 20);
            this.textBoxPathWin.TabIndex = 5;
            this.textBoxPathWin.Text = "D:\\Projects\\efmsoft\\Games\\Puzzler\\Win8.1\\Puzzler\\Puzzler.Shared\\strings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Path to Windows strings folder:";
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(655, 205);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(75, 23);
            this.btnConvert.TabIndex = 13;
            this.btnConvert.Text = "Convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(594, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Base sub folder:";
            // 
            // textBoxLangAndroid
            // 
            this.textBoxLangAndroid.Location = new System.Drawing.Point(683, 33);
            this.textBoxLangAndroid.Name = "textBoxLangAndroid";
            this.textBoxLangAndroid.Size = new System.Drawing.Size(47, 20);
            this.textBoxLangAndroid.TabIndex = 3;
            // 
            // textBoxLangWin
            // 
            this.textBoxLangWin.Location = new System.Drawing.Point(683, 74);
            this.textBoxLangWin.Name = "textBoxLangWin";
            this.textBoxLangWin.Size = new System.Drawing.Size(47, 20);
            this.textBoxLangWin.TabIndex = 7;
            this.textBoxLangWin.Text = "en-us";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(594, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Base sub folder:";
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(12, 388);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(704, 99);
            this.textBoxLog.TabIndex = 14;
            // 
            // listFolders
            // 
            this.listFolders.FormattingEnabled = true;
            this.listFolders.Location = new System.Drawing.Point(28, 162);
            this.listFolders.Name = "listFolders";
            this.listFolders.Size = new System.Drawing.Size(599, 214);
            this.listFolders.TabIndex = 11;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(655, 162);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 12;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Files to convert:";
            // 
            // btnCheckAll
            // 
            this.btnCheckAll.Location = new System.Drawing.Point(131, 128);
            this.btnCheckAll.Name = "btnCheckAll";
            this.btnCheckAll.Size = new System.Drawing.Size(75, 23);
            this.btnCheckAll.TabIndex = 9;
            this.btnCheckAll.Text = "Check all";
            this.btnCheckAll.UseVisualStyleBackColor = true;
            this.btnCheckAll.Click += new System.EventHandler(this.btnCheckAll_Click);
            // 
            // btnCheckNone
            // 
            this.btnCheckNone.Location = new System.Drawing.Point(212, 128);
            this.btnCheckNone.Name = "btnCheckNone";
            this.btnCheckNone.Size = new System.Drawing.Size(75, 23);
            this.btnCheckNone.TabIndex = 10;
            this.btnCheckNone.Text = "Uncheck all";
            this.btnCheckNone.UseVisualStyleBackColor = true;
            this.btnCheckNone.Click += new System.EventHandler(this.btnCheckNone_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 499);
            this.Controls.Add(this.btnCheckNone);
            this.Controls.Add(this.btnCheckAll);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.listFolders);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.textBoxLangWin);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxLangAndroid);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPathWin);
            this.Controls.Add(this.textBoxPathAndroid);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.Text = "Android to Windows strings convertor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPathAndroid;
        private System.Windows.Forms.TextBox textBoxPathWin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxLangAndroid;
        private System.Windows.Forms.TextBox textBoxLangWin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.CheckedListBox listFolders;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCheckAll;
        private System.Windows.Forms.Button btnCheckNone;
    }
}

